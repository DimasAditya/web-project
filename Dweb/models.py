from django.db import models
from django.utils import timezone
from datetime import date

class RegisterModel(models.Model):
	email = models.EmailField(max_length=255)
	password = models.CharField(max_length=255)
	name = models.CharField(max_length=255)
	birthdate = models.DateField()

	class Meta:
		verbose_name = 'Registered User'
		verbose_name_plural = "Registered Users"
	def __str__(self):
		return self.email