from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index,story8,story9

# Create your tests here.
class UnitTest(TestCase):
  def test_url_exist(self):
    response = Client().get('/')
    self.assertEqual(response.status_code, 200)

  def test_function_caller_exist(self):
    found = resolve('/')
    self.assertEqual(found.func, index)

  def test_url_story8(self):
    response = Client().get('/story8')
    self.assertEqual(response.status_code, 200)

  def test_function_caller_story8(self):
    found = resolve('/story8')
    self.assertEqual(found.func, story8)  

  def test_url_story8(self):
    response = Client().get('/story9')
    self.assertEqual(response.status_code, 200)

  def test_function_caller_story8(self):
    found = resolve('/story9')
    self.assertEqual(found.func, story9)  

  # def test_url_story10(self):
  #   response = Client().get('/story8')
  #   self.assertEqual(response.status_code, 200)

  # def test_function_caller_story10(self):
  #   found = resolve('/story8')
  #   self.assertEqual(found.func, story10)
    
  # def test_generate_buku_json(self):
  #   response = Client().get('//')
  #   self.assertEquals(200, response.status_code)



