from django.urls import path
from .views import index, story8,story9,story10, generateBuku, registerUser, validateUser, deleteUser,getUser

urlpatterns = [
    path('', index, name = 'index'),
    path('story8', story8, name = 'story8'),
    path('story9', story9, name = 'story9'),
    path('generatebuku/', generateBuku, name='generatebuku'),
    
    path('story10/', story10, name='register'),
    path('story10/register/', registerUser, name='register_user'),
    path('story10/validate/', validateUser, name='validate_user'),
    path('story10/getuser/', getUser, name='get_user'),
    path('story10/deleteuser/', deleteUser, name='delete_user'),

    # path('story10/getuser/', getUser, name='get_user'),
    # path('story10/deleteuser/', deleteUser, name='delete_user'),

]