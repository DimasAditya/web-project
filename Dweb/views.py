from django.shortcuts import render, redirect
from django.http import JsonResponse
import requests
from .models import RegisterModel
from django.http import JsonResponse
from django.core import serializers
import json

# Create your views here.
def index(request):
	return render(request, 'main.html')

def story8(request):
	return render(request, 'acc.html')

def story9(request):
	return render(request, 'table.html')

def story10(request):
	return render(request, '101.html')

def generateBuku(request):
    URL = "https://www.googleapis.com/books/v1/volumes?q=quilting"
    r = requests.get(URL)
    data = r.json()
    return JsonResponse(data)

def validateUser(request):
    email = request.GET.get("email")
    r = {}
    r["email_exist"] = RegisterModel.objects.filter(email=email).exists()
    return JsonResponse(r)

def registerUser(request):
    email = request.POST.get("email")
    password = request.POST.get("password")
    name = request.POST.get("name")
    birthdate = request.POST.get("birthdate")
    register_success = not RegisterModel.objects.filter(email=email).exists()
    if (register_success):
        user = RegisterModel(email=email, password=password, name=name, birthdate=birthdate)
        user.save()
    r = {"register_success": register_success}
    return JsonResponse(r)

def getUser(request):
    r = {}
    r["user_registered"] = RegisterModel.objects.all().count()
    data = serializers.serialize('json', RegisterModel.objects.all())
    r["users"] = json.loads(data)
    return JsonResponse(r)

def deleteUser(request):
    password = request.POST.get("password")
    pk = request.POST.get("pk")
    user = RegisterModel.objects.all().get(pk=pk)
    print(user)
    r = {}
    r["users"] = json.loads(serializers.serialize('json', [user,]))
    if (password == user.password):
        user.delete()
        r["user_deleted"] = True
    else:
        r["user_deleted"] = False
    return JsonResponse(r)
