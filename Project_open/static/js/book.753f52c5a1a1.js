$(document).ready(function() {
    $("#show-book").click(function() {
        $.ajax({
            url: "/generatebuku/",
            success: function(data) {
                $("#show-book").attr("disabled", true);
                $("#table-container").html("<table class='table table-bordered'><thead><tr><th>Nomor</th><th>Gambar</th><th>Judul</th>" +
                    "<th>Penulis</th><th>Favorite</th></tr></thead><tbody id='output'></tbody></table>")
                var volumeInfo;
                var html;
                for (var i = 0; i < data.items.length; i++) {
                    volumeInfo = data.items[i].volumeInfo;
                    html += "<tr>";
                    html += "<td class='align-middle text-center'>" + (i + 1) + "</td>";
                    html += "<td class='align-middle text-center'>" + "<img src='" + volumeInfo.imageLinks.thumbnail + "'>" +"</td>";
                    html += "<td class='align-middle text-center'>" + volumeInfo.title + "</td>";
                    html += "<td class='align-middle text-center'>";
                    for (var j = 0; j < volumeInfo.authors.length; j++) {
                        html += volumeInfo.authors[j];
                    }
                    html += "</td>";
                    html +="<td class='align-middle text-center'><p class='fa fa-star'></p></td>";
                    html += "</tr>";
                }
                $("#output").append(html);
                $(".fa-star").click(function() {
                    if ($(this).hasClass("checked")) {
                        $(this).removeClass("checked");
                        var newVal = parseInt($("#counter").text()) - 1
                        $("#counter").text(newVal)
                    } else {
                        $(this).addClass("checked");
                        var newVal = parseInt($("#counter").text()) + 1
                        $("#counter").text(newVal)
                    }
                })
            }
        })
    })
})