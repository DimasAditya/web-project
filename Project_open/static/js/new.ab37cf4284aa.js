function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}


$(document).ready(function() {
    var validateSuccess;

    $("#show-user").click(function() {
        $.ajax({
            url: "/story10/getuser/",
            success: function(data) {
                $("#user-table").hide();
                var html = "";
                if (data.users.length == 0) {
                    html += "<h1>No User Registered</h1>"
                    $("#user-table").html(html);
                    $("#user-table").fadeIn("normal");
                    return;
                }
                html ="<table class='table table-bordered'><tr><th>Email</th><th>Name</th><th>Birth Date</th><th>Unsubscribe</th></tr>";
                for(var i = 0; i < data.users.length; i++) {
                    html += "<tr>"
                    html += "<td>" + data.users[i].fields.email + "</td>";
                    html += "<td>" + data.users[i].fields.name + "</td>";
                    html += "<td>" + data.users[i].fields.birthdate + "</td>";
                    html += "<td><button class='btn btn-danger' onclick='deleteUser(" + data.users[i].pk + ")'>Unsubscribe</button></td>";
                    html += "</tr>"
                }
                html += "</table>"
                $("#user-table").html(html);
                $("#user-table").fadeIn("normal");
            }
        })
    })

    $("#show-user").trigger("click");

    $("#email").on("input", function() {
        if (validateSuccess) {
            $("#submit-register").addClass("disabled");
            $("#submit-register").attr("disabled", true);
            $("#message-container").html("<div class='alert alert-info'>Data Changed. Validate Again!</div>");
            validateSuccess = false;
        }
    })

    $("#validate-button").on("click", function() {
        if ($("form").valid()) {
            $.ajax({
                url: "/story10/validate/",
                data: {email: $("#email").val()},
                success: function(data) {
                    $("#message-container").hide();
                    var html = "";
                    var validationSuccess = true;
                    if (data.email_exist) {
                        html += "<div class='alert alert-danger'>Email Registered</div>";
                        validationSuccess = false;
                    } else {
                        html += "<div class='alert alert-success'>Email Validated</div>";
                    }
    
                    if (validationSuccess) {
                        validateSuccess = true;
                        $("#submit-register").removeClass("disabled");
                        $("#submit-register").attr("disabled", false);
                    } else {
                        $("#submit-register").addClass("disabled");
                        $("#submit-register").attr("disabled", true);
                    }
    
                    $("#message-container").html(html);
                    $("#message-container").fadeIn("normal");
                }
            })
        }
    })

    $("form").submit(function(e) {
        e.preventDefault();
        var data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "/story10/register/",
            data: data,
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            success: function(data) {
                $("#message-container").hide();
                if (data.register_success) {
                    var html = "<div class='alert alert-success'>Successfully Registered</div>";
                } else {
                    var html = "<div class='alert alert-danger'>Register Failed</div>";
                }
                $("#message-container").html(html);
                $("#message-container").fadeIn("slow");
                $("form").trigger("reset");
                $("#show-user").trigger("click");
            }
        })
    })
})

function deleteUser(pk) {
    var password = window.prompt("Enter your password");
    $.ajax({
        type: "POST",
        url: "/story10/deleteuser/",
        data: {"pk": pk, "password": password},
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
        success: function(data) {
            if (data.user_deleted) {
                alert("User successfully deleted");
                $("#show-user").trigger("click");
            } else {
                alert("Password not match!");
            }
        }
    })
}