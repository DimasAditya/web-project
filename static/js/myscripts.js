var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}

function gantiWarnaHitam(){
    document.getElementById("body").style.background="black";
    document.getElementById("tombol").style.background="brown";
}
function gantiWarnaMerah(){
    document.getElementById("body").style.background="brown";
    document.getElementById("tombol").style.background="black";
}
function gantiWarnaDefault(){
    document.getElementById("body").style.background="white";
    document.getElementById("tombol").style.background="rgb(44, 34, 177)";
  }

/*
* === chalenge loading ===
*/
$body = $("body");

$(document).on({
    ajaxStart: function() { $body.addClass("loading");    },
     ajaxStop: function() { $body.removeClass("loading"); }    
});